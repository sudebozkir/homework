def primesum(A):
    def is_prime(num):
        if num <= 1:
            return False
        for i in range(2, int(num ** 0.5) + 1):
            if num % i == 0:
                return False
        return True

    for i in range(2, A // 2 + 1):
        if is_prime(i) and is_prime(A - i):
            return [i, A - i]

A = 4
result = primesum(A)
print(result)

#Correct, good
# but notice, a def inside an another def is not a good design.
