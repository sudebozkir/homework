def calculate_board_size(w, h, n):
    max_size = max(w, h)
    board_size = max_size
    while True:
        rows = board_size // h
        cols = board_size // w
        total_diplomas = rows * cols
        if total_diplomas >= n:
            return board_size
        board_size += 1

w, h, n = map(int, input().split())
result = calculate_board_size(w, h, n)
print(result)