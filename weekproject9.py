def min_elements_to_flip(arr):
    total_sum = sum(arr)
  #Total_sum is the sum of the numbers in the array.
  #Calculate the approximate non-negative sum of the array (half of total_sum)  
     n = len(arr)
    dp = [[False] * (total_sum + 1) for _ in range(n + 1)]
    dp[0][0] = True
    
    for i in range(1, n + 1):
        for j in range(total_sum + 1):
            if j >= arr[i-1]:
                dp[i][j] = dp[i-1][j] or dp[i-1][j - arr[i-1]]
            else:
                dp[i][j] = dp[i-1][j]
    closest_sum = 0
    for j in range(total_sum // 2 + 1):
        if dp[n][j]:
            closest_sum = j
    min_non_negative_sum = total_sum - 2 * closest_sum
    flips = 0
    remaining_sum = total_sum - closest_sum
    i = n
    while closest_sum > 0 and i > 0:
        if not dp[i-1][closest_sum]:
            flips += 1
            closest_sum -= arr[i-1]
        i -= 1
    return flips
arr = [1, 2, 3, 4, 5]
print(min_elements_to_flip(arr))