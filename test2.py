def split_stones(stones, index, sum1, sum2):
    if index == len(stones):
        return abs(sum1 - sum2)

    return min(
        split_stones(stones, index + 1, sum1 + stones[index], sum2),
        split_stones(stones, index + 1, sum1, sum2 + stones[index])
    )


N = int(input("Enter the number of stones: "))
stones = list(map(int, input("Enter the weights of the stones separated by space: ").split()))

result = split_stones(stones, 0, 0, 0)
print(result)