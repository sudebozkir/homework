from typing import List

class Solution:
    def sortPeople(self, names: List[str], heights: List[int]) -> List[str]:
        name_height_pairs = []
        for i in range(len(names)):
            name_height_pairs.append((names[i], heights[i]))
        
        name_height_pairs.sort(key=lambda x: x[1], reverse=True)
        sorted_names = [pair[0] for pair in name_height_pairs]
        
        return sorted_names
